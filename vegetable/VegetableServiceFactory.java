package vegetable;

import java.util.Properties;
import java.util.Scanner;

public abstract class VegetableServiceFactory {

	public static VegetableService getVegetableservice(String type ,Properties properties)
	
	{
		Scanner scanner=new Scanner(System.in);
		
//		String type=properties.getProperty("implementation");
		VegetableService service = null;
		switch(type) {
		case "array":
		{
			service =new VegetableArrayImpl();
			break;
		}
		case "ArrayList":
		{
			service=new VegitableArrayListImpl();
			break;
		}
		
		case "Database": 
		{
			String user=properties.getProperty("user");
			String url=properties.getProperty("url");
			String driver=properties.getProperty("driver");
			String password=properties.getProperty("password");
			service=new VegitableDatabaseImpl(driver,url,user,password);
			
		break;
		}
	
	}
		return service;
	}

}

