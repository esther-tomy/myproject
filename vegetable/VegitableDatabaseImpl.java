package vegetable;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

public class VegitableDatabaseImpl implements VegetableService {

	String driver, url, user;
	String password;
	private Vegetable vegetable;

	public VegitableDatabaseImpl(String driver, String url, String user, String password) {

		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, password);

	}

	private Vegetable getVegetable(ResultSet resultset) throws SQLException {
		Vegetable vegetable = new Vegetable();
		vegetable.setName(resultset.getString("name"));
		vegetable.setPrice(resultset.getInt("veg_price"));
		vegetable.setWeight(resultset.getInt("veg_weight"));

		vegetable.setDate(resultset.getDate("expiry_date"));

		return vegetable;
	}

	@Override
	public void addVegetable(Vegetable vegetable)  {
		
		try {
			
			Connection connection = getConnection();
			String query = "INSERT INTO vegetable(name,veg_price,veg_weight,expiry_date) VALUES (?,?,?,?)";
			PreparedStatement prepareStatement = connection.prepareStatement(query);

			prepareStatement.setString(1, vegetable.getName());
			prepareStatement.setInt(2, vegetable.getPrice());
			prepareStatement.setInt(3, vegetable.getWeight());
			prepareStatement.setDate(4, Date.valueOf(vegetable.getDateofexpiry()));
			prepareStatement.executeUpdate();
			connection.close();
			System.out.println("successfully inserted");
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
	}

	@Override
	public void displayAll() {
		// TODO Auto-generated method stub

		try {

			Connection connection = getConnection();
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM vegetable;");
			ResultSet resultset = prepareStatement.executeQuery();
			while (resultset.next()) {

				Vegetable vegetable = getVegetable(resultset);
				System.out.println(vegetable);

			}
			resultset.close();
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void search(String name1) throws Exception {

		try {
			Connection connection;
			connection = getConnection();
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM vegetable where name=?");
			prepareStatement.setString(1, name1);
			ResultSet resultset = prepareStatement.executeQuery();
			while (resultset.next())

			{
				Vegetable vegetable = getVegetable(resultset);
				System.out.println(vegetable);

			}
			resultset.close();
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {

			e.printStackTrace();
		}
		

	}

	@Override
	public List<Vegetable> search(LocalDate dateOfExpiry) {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
			connection = getConnection();
			// String query = "SELECT * FROM player;";
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT * FROM vegetable where expiry_date=?;");

			preparedStatement.setDate(4, Date.valueOf(vegetable.getDateofexpiry()));
			;
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Vegetable vegetable = getVegetable(resultSet);
				System.out.println(vegetable);
			}
			connection.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public void sort() {

		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from vegetable order by price asc");
			ResultSet resultSet = prestatement.executeQuery();
			while (resultSet.next()) {
				Vegetable customer = getVegetable(resultSet);
				System.out.println(customer);

			}
			resultSet.close();
			connection.close();

		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	
	@Override
	public void download() throws IOException {
		
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from vegetable");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("Coffee database details.csv");
			while (resultSet.next()) {

				String data = convertTocsv(getVegetable(resultSet));
				file.write(data);
			}
			file.close();
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	@Override
	public void downloadJSON() throws IOException {
		

		Connection connection;

		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from vegetable");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("downloadjsonfile.json");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while (resultSet.next()) {
				String data = join.add(getJSON(getVegetable(resultSet))).toString();
				file.write(data);

			}
			file.close();
			connection.close();
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}

	}

	@Override
	public void delete(String s) throws ClassNotFoundException, SQLException {
		

		Connection connection = getConnection();
		try {

			PreparedStatement prepareStatement = connection.prepareStatement("delete  from vegetable  where name=?");
			prepareStatement.setString(1, s);
			prepareStatement.execute();

			connection.close();
		}

		catch (SQLException e) {
			e.printStackTrace();

		}

	}

	
}
