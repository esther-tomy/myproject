package vegetable;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.Spring;

public class VegitableArrayListImpl implements VegetableService {

	List<Vegetable> vegitablearraylist = new ArrayList<>();

	@Override
	public void displayAll() {

		vegitablearraylist.forEach(System.out::println);

		}

	@Override
	public void search(String name) {

		System.out.println(vegitablearraylist.stream().filter(vegetable -> vegetable.getName().equalsIgnoreCase(name))
				.collect(Collectors.toList()));

	}

	@Override
	public void sort() {

		Collections.sort(vegitablearraylist);
		displayAll();

	}


	@Override
	public void downloadJSON() throws IOException {

		String data;
		FileWriter file = new FileWriter("downloadjsonfile.json");
		for (int i = 0; i < vegitablearraylist.size(); i++) {
			if (i == 0)
				data = "[" + getJSON(vegitablearraylist.get(i));
			else
				data = "," + getJSON(vegitablearraylist.get(i));
			if (i == vegitablearraylist.size() - 1)
				data = data + "]";
			file.write(data);
		}
		file.close();
	}

	@Override
	public void download() throws IOException {

		try {
			FileWriter file = new FileWriter("download.csv");
			for (Vegetable data : vegitablearraylist) {
				file.write(convertTocsv(data));
				System.out.println(data);
			}
			file.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public List<Vegetable> search(LocalDate checkDate) {

		return vegitablearraylist.stream()
				.filter(vegetableList -> ((Vegetable) vegitablearraylist).getDateofexpiry().isAfter(checkDate))
				.collect(Collectors.toList());

	}

	@Override
	public void delete(String s) {

		



			for (Iterator<Vegetable> iter = vegitablearraylist.iterator(); iter.hasNext();) {
			Vegetable vegetable = iter.next();
			if (vegetable.getName().equals(s)) {
			iter.remove();
			}
		
			}

	}

	@Override
	public void addVegetable(Vegetable vegetable)  {

		if (vegitablearraylist.size() > 0 && vegitablearraylist.contains(vegetable)) {
			System.out.println("Already exist");

		}

		else {
			vegitablearraylist.add(vegetable);
		}

//		
	}

	

}
